const custom = {
    asyncSetForeach : async function<T>(set: Set<T>, callback: Function) {
        for (let val of set) {
            await callback(val);
        }
    }
}

export = custom;