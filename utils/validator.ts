export default function (requirements: Array<string>, req: object, res: object, next: Function): Function {
    for (let i = 0; i < requirements.length; i += 1) {
        if (typeof req.body[requirements[i]] === "undefined") {
            return res.status(400).send({
                error: 'param ' + requirements[i] + ' is missing'
            });
        }
    }

    return next();
}