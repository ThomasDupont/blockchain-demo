import * as assert from 'assert';
import { asyncSetForeach } from '../utils/customMethods';

const waitFor = (ms) => new Promise(r => setTimeout(r, ms));

describe('Test asyncSetForeach', function () {
    it('should wait result', function (done) {
        const set = new Set([1, 2, 3]);
        let i = 0;
        asyncSetForeach(set, async (e) => {
            await waitFor(50);
            i++;
        }).then(() => {
            assert(i === 3);
            done();
        }).catch(e => done(e));
    })
})