import * as crypto from 'crypto';
import { get } from 'request-promise';
import Block from "./interfaces/block";
import Transaction from "./interfaces/transaction";
import { asyncSetForeach } from './utils/customMethods';

export default class Blockchain {
    public chain: Array<Block>;
    public currentTransaction: Array<Transaction>;
    public nodes: Set<string>;

    constructor() {
        this.chain = [];
        this.currentTransaction = [];
        this.nodes = new Set();

        this.newBlock(100, '1');
    }

    /**
     * Create a new Block in the Blockchain
     *
     * @param proof
     * @param previousHash
     */
    public newBlock(proof: number, previousHash: string = null): Block
    {
        const block: Block = {
            index: this.chain.length + 1,
            timestamp: Date.now(),
            transactions: this.currentTransaction,
            proof,
            previousHash: previousHash || Blockchain.hash(this.chain[this.chain.length -1])
        }

        this.currentTransaction = [];
        this.chain.push(block);

        return block;
    }

    /**
     * Creates a new transaction to go into the next mined Block
     *
     * @param sender
     * @param recipient
     * @param amount
     */
    public newTransaction(sender: string, recipient: string, amount: number): number
    {
        this.currentTransaction.push({ sender, recipient, amount });

        return this.chain.length + 1;
    }

    /**
     * Simple Proof of Work Algorithm:
     - Find a number p' such that hash(pp') contains leading 4 zeroes, where p is the previous p'
     - p is the previous proof, and p' is the new proof

     * @param lastProof
     */
    public proofOfWork(lastProof: number): number
    {
        let proof = 0;
        while (!Blockchain.validProof(lastProof, proof)) {
            proof += 1;
        }

        return proof;
    }

    /**
     * Add a new node to the list of nodes
     *
     * @param address
     */
    public registerNode (address: URL): void
    {
        this.nodes.add(address.host);
    }

    /**
     * Determine if a given blockchain is valid
     * @param chain
     */
    public validChain(chain: Array<Block>): boolean
    {
        let lastBlock = chain[0];
        let currentIndex = 1;

        while (currentIndex < chain.length) {
            const block = chain[currentIndex];
            console.log(lastBlock);
            console.log(block);
            console.log("\n-----------\n");
            //Check that the hash of the block is correct
            if (block.previousHash != Blockchain.hash(lastBlock)) {
                return false;
            }

            //Check that the Proof of Work is correct
            if (!Blockchain.validProof(lastBlock.proof, block.proof)) {
                return false;
            }

            lastBlock = block;
            currentIndex += 1;
        }

        return true
    }

    /**
     * This is our Consensus Algorithm, it resolves conflicts
     by replacing our chain with the longest one in the network.
     */
    public resolveConflicts(): Promise<boolean>
    {
        const neighbours: Set<string> = this.nodes;
        let newChain: Array<Block> | null = null;
        //We're only looking for chains longer than ours
        let maxLength: number = this.chain.length;

        //Grab and verify the chains from all the nodes in our network
        return asyncSetForeach(neighbours, async (node: string) => {
            let response: {
                length: number,
                chain: Array<Block>
            };

            try {
                response = await get('http://'+node+'/chain');

                //Check if the length is longer and the chain is valid
                if (response.length > maxLength && this.validChain(response.chain)) {
                    maxLength = response.length;
                    newChain = response.chain;
                }
            } catch (e) {
                console.error(e);
            }
        }).then(() => {
            //Replace our chain if we discovered a new, valid chain longer than ours
            if (newChain) {
                this.chain = newChain;

                return Promise.resolve(true);
            }

            return Promise.resolve(false)
        });
    }

    /**
     * Creates a SHA-256 hash of a Block
     *
     * @param block
     */
    static hash(block: Block): string
    {
        return crypto.createHash('sha256').update(JSON.stringify(block)).digest('hex');
    }

    /**
     * Validates the Proof: Does hash(last_proof, proof) contain 4 leading zeroes?
     *
     * @param lastProof
     * @param proof
     */
    static validProof(lastProof: number, proof: number): boolean
    {
        const guessHash = crypto.createHash('sha256').update(lastProof.toString() + proof.toString()).digest('hex');

        return guessHash.slice(-4) === "0000";
    }
}
