# Blockchain demo

Based on https://medium.com/@vanflymen/learn-blockchains-by-building-one-117428612f46

## Start

### Docker

- docker-compose up
- Postman on localhost:3000

### Local

- Have Node.Js 10
- npm install
- npm run start-dev