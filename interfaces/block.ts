import Transaction from "./transaction";

export default interface Block {
    index: number,
    timestamp: number,
    transactions: Array<Transaction>,
    proof: number,
    previousHash: string
}
