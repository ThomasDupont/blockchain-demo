import * as express from 'express';
import * as bodyParser from 'body-parser';
import Blockchain from "./blockchain";
import Transaction from "./interfaces/transaction";
import validator from "./utils/validator";

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const blockchain = new Blockchain();
const nodeIdentifier = '9F58C5C3CD9A498699807F05C7D21BDB';

app.post('/transactions/new', validator.bind(this, ['sender', 'recipient', 'amount']), (req, res) => {
    const { sender, recipient, amount }: Transaction = req.body;
    const index = blockchain.newTransaction(sender, recipient, amount);
    return res.send({
        message: 'Transaction will be added to Block ' + index
    });
});

app.get('/mine', (req, res) => {
    const lastBlock = blockchain.chain[blockchain.chain.length -1];
    const lastProof = lastBlock.proof;
    const proof = blockchain.proofOfWork(lastProof);

    blockchain.newTransaction("0", nodeIdentifier, 1);

    const previousHash = Blockchain.hash(lastBlock);
    const block = blockchain.newBlock(proof, previousHash);

    const response = {
        message: "New Block Forged",
        ...block,
    }
    res.send(response);
});

app.get('/chain', (req, res) => res.send({
    chain: blockchain.chain,
    length: blockchain.chain.length,
}));

app.post('/nodes/register', validator.bind(this, ['nodes']), (req, res) => {
    const nodes = req.body.nodes;
    nodes.forEach((node : string) => {
        blockchain.registerNode(new URL(node));
    });

    const response = {
        message: 'New nodes have been added',
        totalNodes: blockchain.nodes.size,
    }

    return  res.send(response);
});

app.get('/nodes/resolve', (req, res) => {
    return blockchain.resolveConflicts().then((replaced: boolean) => {
        const response = replaced ? {
            message: 'Our chain was replaced',
            newChain: blockchain.chain
        } : {
            message: 'Our chain is authoritative',
            chain: blockchain.chain
        };

        return res.send(response);
    });
});

app.use((req, res) => res.status(400).send({
    error: 'resource_not_found'
}))

app.listen(3000, () => console.info('launch'));